package com.springMicrofin360.microfin.controller;

import com.springMicrofin360.microfin.model.WorkingAreaModel;
import com.springMicrofin360.microfin.service.DivisionService;
import com.springMicrofin360.microfin.service.WorkingAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class WorkingAreaController {
    @Autowired
    private WorkingAreaService workingAreaService;
    @Autowired
    private DivisionService divisionService;

    @RequestMapping(value = "/workingArea", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("workingAreaList",workingAreaService.getAllWorkingAreas());
        return "workingArea/index";
    }
    @RequestMapping(path = "/workingArea/add")
    public String add(Model model){
        model.addAttribute("workingArea",new WorkingAreaModel());
        if (divisionService.getAllDivisions() != null) {
            model.addAttribute("divisionList",divisionService.getAllDivisions());
        }

        return "workingArea/save";
    }

    @GetMapping(path = "/workingArea/edit/{id}")
    public String edit(Model model, @PathVariable int id){
        WorkingAreaModel workingAreaModel = workingAreaService.findWorkingAreaById(id);
        model.addAttribute("workingArea",workingAreaModel);
        if (divisionService.getAllDivisions() != null) {
            model.addAttribute("divisionList",divisionService.getAllDivisions());
        }
        return "workingArea/save";
    }
    @PostMapping("/workingArea")
    public String villageOrBlockSave(@RequestParam(name="branchId") int branchId,@RequestParam(name="workingAreaId") int workingAreaId,@RequestParam(name="villageOrBlock") int villageOrBlockId,@RequestParam(name = "workingAreaName") String workingAreaName,@RequestParam(name = "workingAreaCode") String workingAreaCode,Model model ) {
        WorkingAreaModel workingAreaModel = new WorkingAreaModel();
        workingAreaModel.setBranchId(branchId);
        workingAreaModel.setVillageOrBlockId(villageOrBlockId);
        workingAreaModel.setWorkingAreaId(workingAreaId);
        workingAreaModel.setWorkingAreaName(workingAreaName);
        workingAreaModel.setWorkingAreaCode(workingAreaCode);
        if(workingAreaId > 0){
            workingAreaModel.setWorkingAreaId(workingAreaId);
            workingAreaService.editWorkingArea(workingAreaModel);
        }else{
            if (workingAreaModel != null) {
                workingAreaService.addWorkingArea(workingAreaModel);
            }

        }
        model.addAttribute("workingAreaList",workingAreaService.getAllWorkingAreas());
        return "workingArea/index";
    }

    @GetMapping(path = "/workingArea/delete/{id}")
    public String delete(Model model,@PathVariable int id){
        workingAreaService.deleteWorkingArea(id);
        model.addAttribute("workingAreaList",workingAreaService.getAllWorkingAreas());
        return "workingArea/index";
    }

}
