package com.springMicrofin360.microfin.controller;

import com.springMicrofin360.microfin.model.DistrictModel;
import com.springMicrofin360.microfin.service.DistrictService;
import com.springMicrofin360.microfin.service.DivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class DistrictController {
    @Autowired
    private DistrictService districtService;
    @Autowired
    private DivisionService divisionService;
    @RequestMapping(value = "/district", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("districtList",districtService.getAllDistricts());
        /*List<DistrictModel> districts = districtService.getAllDistricts();
        for(DistrictModel district : districts){
            System.out.println(district.getDistrictId());
            System.out.println(" ");
            System.out.println(district.getDivisionId());
            System.out.println(" ");
            System.out.println(district.getDistrictName());
        }*/
        return "district/index";
    }
    @RequestMapping(path = "/district/add")
    public String add(Model model){
        model.addAttribute("district",new DistrictModel());
        if (divisionService.getAllDivisions() != null) {
            model.addAttribute("divisionList",divisionService.getAllDivisions());
        }
        return "district/save";
    }

    @GetMapping(path = "/district/edit/{id}")
    public String edit(Model model, @PathVariable int id){
        DistrictModel districtModel = districtService.findDistrictById(id);
        model.addAttribute("district",districtModel);
        if (divisionService.getAllDivisions() != null) {
            model.addAttribute("divisionList",divisionService.getAllDivisions());
        }
        return "district/save";
    }
    @PostMapping("/district")
    public String districtSave(@RequestParam(name="districtId") int districtId,@RequestParam(name="division") int divisionId,@RequestParam(name = "districtName") String districtName,Model model) {
        DistrictModel districtModel = new DistrictModel();
        districtModel.setDivisionId(divisionId);
        districtModel.setDistrictName(districtName);
        if(districtId > 0){
            districtModel.setDistrictId(districtId);
            districtService.editDistrict(districtModel);
        }else{
            if (districtModel != null) {
                districtService.addDistrict(districtModel);
            }

        }
        model.addAttribute("districtList",districtService.getAllDistricts());
        return "district/index";
    }

    @GetMapping(path = "/district/delete/{id}")
    public String delete(Model model,@PathVariable int id){
        districtService.deleteDistrict(id);
        model.addAttribute("districtList",districtService.getAllDistricts());
        return "district/index";
    }

    @RequestMapping(value="/district/findByDivision", method=RequestMethod.GET)
    public @ResponseBody List<DistrictModel> findDistrictByDivisionId(@RequestParam("divisionId") int id) {
        /*System.out.println(id);
        for (DistrictModel district:districtService.findDistrictByDivisionId(id)
             ) {
            System.out.println(district.getDistrictName());
        }*/
        return districtService.findDistrictByDivisionId(id);
    }

}
