package com.springMicrofin360.microfin.controller;

import com.springMicrofin360.microfin.model.ThanaModel;
import com.springMicrofin360.microfin.service.DistrictService;
import com.springMicrofin360.microfin.service.DivisionService;
import com.springMicrofin360.microfin.service.ThanaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ThanaController {
    @Autowired
    private ThanaService thanaService;
    @Autowired
    private DivisionService divisionService;
    @Autowired
    private DistrictService districtService;
    @RequestMapping(value = "/thana", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("thanaList",thanaService.getAllThanas());
        return "thana/index";
    }
    @RequestMapping(path = "/thana/add")
    public String add(Model model){
        model.addAttribute("thana",new ThanaModel());
        if (divisionService.getAllDivisions() != null) {
            model.addAttribute("divisionList",divisionService.getAllDivisions());
        }
        return "thana/save";
    }

    @GetMapping(path = "/thana/edit/{id}")
    public String edit(Model model, @PathVariable int id){
        ThanaModel thanaModel = thanaService.findThanaById(id);
        model.addAttribute("thana",thanaModel);
        if (divisionService.getAllDivisions() != null) {
            model.addAttribute("divisionList",divisionService.getAllDivisions());
        }
        return "thana/save";
    }
    @PostMapping("/thana")
    public String thanaSave(@RequestParam(name="thanaId") int thanaId,@RequestParam(name="division") int divisionId,@RequestParam(name = "thanaName") String thanaName,@RequestParam(name="district") int districtId,Model model) {
        ThanaModel thanaModel = new ThanaModel();
        thanaModel.setThanaId(thanaId);
        thanaModel.setThanaName(thanaName);
        thanaModel.setDistrictId(districtId);
        thanaModel.setDivisionId(divisionId);
        if(thanaId > 0){
            thanaModel.setThanaId(thanaId);
            thanaService.editThana(thanaModel);
        }else{
            if (thanaModel != null) {
               thanaService.addThana(thanaModel);
            }

        }
        model.addAttribute("thanaList",thanaService.getAllThanas());
        return "thana/index";
    }

    @GetMapping(path = "/thana/delete/{id}")
    public String delete(Model model,@PathVariable int id){
        thanaService.deleteThana(id);
        model.addAttribute("thanaList",thanaService.getAllThanas());
        return "thana/index";
    }
    @RequestMapping(value="/thana/findByDistrict", method=RequestMethod.GET)
    public @ResponseBody
    List<ThanaModel> findThanaByDistrictId(@RequestParam("districtId") int id) {
        /*System.out.println(id);
        for (ThanaModel thana:thanaService.findThanaByDistrictId(id)
             ) {
            System.out.println(thana.getThanaName());
        }*/
        return thanaService.findThanaByDistrictId(id);
    }

}
