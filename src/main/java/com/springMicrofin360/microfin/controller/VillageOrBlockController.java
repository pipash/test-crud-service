package com.springMicrofin360.microfin.controller;
import com.springMicrofin360.microfin.model.VillageOrBlockModel;
import com.springMicrofin360.microfin.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class VillageOrBlockController {
    @Autowired
    private VillageOrBlockService villageOrBlockService;
    @Autowired
    private DivisionService divisionService;

    @RequestMapping(value = "/villageOrBlock", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("villageOrBlockList",villageOrBlockService.getAllVillageOrBlocks());
        return "villageOrBlock/index";
    }
    @RequestMapping(path = "/villageOrBlock/add")
    public String add(Model model){
        model.addAttribute("villageOrBlock",new VillageOrBlockModel());
        if (divisionService.getAllDivisions() != null) {
            model.addAttribute("divisionList",divisionService.getAllDivisions());
        }
        return "villageOrBlock/save";
    }

    @GetMapping(path = "/villageOrBlock/edit/{id}")
    public String edit(Model model, @PathVariable int id){
        VillageOrBlockModel villageOrBlockModel = villageOrBlockService.findVillageOrBlockById(id);
        model.addAttribute("villageOrBlock",villageOrBlockModel);
        if (divisionService.getAllDivisions() != null) {
            model.addAttribute("divisionList",divisionService.getAllDivisions());
        }
        return "villageOrBlock/save";
    }
    @PostMapping("/villageOrBlock")
    public String villageOrBlockSave(@RequestParam(name="villageOrBlockId") int villageOrBlockId,@RequestParam(name="unionOrWard") int unionOrWardId,@RequestParam(name="division") int divisionId,@RequestParam(name = "villageOrBlockName") String villageOrBlockName,@RequestParam(name="district") int districtId,@RequestParam(name="thana") int thanaId, Model model ) {
        VillageOrBlockModel villageOrBlockModel = new VillageOrBlockModel();
        villageOrBlockModel.setThanaId(thanaId);
        villageOrBlockModel.setUnionOrWardId(unionOrWardId);
        villageOrBlockModel.setDistrictId(districtId);
        villageOrBlockModel.setDivisionId(divisionId);
        villageOrBlockModel.setVillageOrBlockId(villageOrBlockId);
        villageOrBlockModel.setVillageOrBlockName(villageOrBlockName);
        if(villageOrBlockId > 0){
            villageOrBlockModel.setVillageOrBlockId(villageOrBlockId);
            villageOrBlockService.editVillageOrBlock(villageOrBlockModel);
        }else{
            if (villageOrBlockModel != null) {
                villageOrBlockService.addVillageOrBlock(villageOrBlockModel);
            }

        }
        model.addAttribute("villageOrBlockList",villageOrBlockService.getAllVillageOrBlocks());
        return "villageOrBlock/index";
    }

    @GetMapping(path = "/villageOrBlock/delete/{id}")
    public String delete(Model model,@PathVariable int id){
        villageOrBlockService.deleteVillageOrBlock(id);
        model.addAttribute("villageOrBlockList",villageOrBlockService.getAllVillageOrBlocks());
        return "villageOrBlock/index";
    }

    @RequestMapping(value="/villageOrBlock/findByUnionOrWard", method=RequestMethod.GET)
    public @ResponseBody
    List<VillageOrBlockModel> findByUnionOrWard(@RequestParam("unionOrWardId") int id) {
        /*System.out.println(id);
        for (UnionOrWardModel unionOrWardModel:unionOrWardService.findUnionOrWardByThanaId(id)
             ) {
            System.out.println(unionOrWardModel.getUnionOrWardName());
        }*/
        return villageOrBlockService.findVillageOrBlockByUnionOrWard(id);
    }
}
