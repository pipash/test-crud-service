package com.springMicrofin360.microfin.controller;

import com.springMicrofin360.microfin.model.UnionOrWardModel;
import com.springMicrofin360.microfin.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UnionOrWardController {
    @Autowired
    private UnionOrWardService unionOrWardService;
    @Autowired
    private DivisionService divisionService;

    @RequestMapping(value = "/unionOrWard", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("unionOrWardList",unionOrWardService.getAllUnionOrWards());
        return "unionOrWard/index";
    }
    @RequestMapping(path = "/unionOrWard/add")
    public String add(Model model){
        model.addAttribute("unionOrWard",new UnionOrWardModel());
        if (divisionService.getAllDivisions() != null) {
            model.addAttribute("divisionList",divisionService.getAllDivisions());
        }
        return "unionOrWard/save";
    }

    @GetMapping(path = "/unionOrWard/edit/{id}")
    public String edit(Model model, @PathVariable int id){
        UnionOrWardModel unionOrWardModel = unionOrWardService.findUnionOrWardById(id);
        model.addAttribute("unionOrWard",unionOrWardModel);
        if (divisionService.getAllDivisions() != null) {
            model.addAttribute("divisionList",divisionService.getAllDivisions());
        }
        return "unionOrWard/save";
    }
    @PostMapping("/unionOrWard")
    public String unionOrWardSave(@RequestParam(name="unionOrWardId") int unionOrWardId,@RequestParam(name="division") int divisionId,@RequestParam(name = "unionOrWardName") String unionOrWardName,@RequestParam(name="district") int districtId,@RequestParam(name="thana") int thanaId, Model model ) {
        UnionOrWardModel unionOrWardModel = new UnionOrWardModel();
        unionOrWardModel.setThanaId(thanaId);
        unionOrWardModel.setUnionOrWardId(unionOrWardId);
        unionOrWardModel.setUnionOrWardName(unionOrWardName);
        unionOrWardModel.setDistrictId(districtId);
        unionOrWardModel.setDivisionId(divisionId);
        if(unionOrWardId > 0){
            unionOrWardModel.setUnionOrWardId(unionOrWardId);
            unionOrWardService.editUnionOrWard(unionOrWardModel);
        }else{
            if (unionOrWardModel != null) {
                unionOrWardService.addUnionOrWard(unionOrWardModel);
            }

        }
        model.addAttribute("unionOrWardList",unionOrWardService.getAllUnionOrWards());
        return "unionOrWard/index";
    }

    @GetMapping(path = "/unionOrWard/delete/{id}")
    public String delete(Model model,@PathVariable int id){
        unionOrWardService.deleteUnionOrWard(id);
        model.addAttribute("unionOrWardList",unionOrWardService.getAllUnionOrWards());
        return "unionOrWard/index";
    }

    @RequestMapping(value="/unionOrWard/findByThana", method=RequestMethod.GET)
    public @ResponseBody
    List<UnionOrWardModel> findByThana(@RequestParam("thanaId") int id) {
        System.out.println(id);
        for (UnionOrWardModel unionOrWardModel:unionOrWardService.findUnionOrWardByThanaId(id)
             ) {
            System.out.println(unionOrWardModel.getUnionOrWardName());
        }
        return unionOrWardService.findUnionOrWardByThanaId(id);
    }
}
