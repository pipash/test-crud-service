package com.springMicrofin360.microfin.repository;

import com.springMicrofin360.microfin.model.EmployeeDepartmentModel;
import com.springMicrofin360.microfin.model.rowmapper.EmployeeDepartmentRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeDepartmentRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void addEmployeeDepartMent(EmployeeDepartmentModel employeeDepartmentModel){
        String query = "INSERT INTO employee_departments(name,code) VALUES(?,?)";
        jdbcTemplate.update(query, employeeDepartmentModel.getDepartmentName(),employeeDepartmentModel.getDepartmentCode());
    }

    public List<EmployeeDepartmentModel> getAllEmployeeDepaerments() {
        String Sql = "SELECT * FROM employee_departments";
        RowMapper<EmployeeDepartmentModel> rowMapper = new EmployeeDepartmentRowMapper();
        try{
            List<EmployeeDepartmentModel> departments = jdbcTemplate.query(Sql, rowMapper);
            return departments;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }
}
