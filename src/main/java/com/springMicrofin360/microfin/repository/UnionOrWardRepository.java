package com.springMicrofin360.microfin.repository;

import com.springMicrofin360.microfin.model.UnionOrWardModel;
import com.springMicrofin360.microfin.model.rowmapper.UnionOrWardRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UnionOrWardRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    public void addUnionOrWard(UnionOrWardModel unionOrWardModel) {
        String query = "INSERT INTO po_unions_or_wards(division_id,district_id,thana_id,name) VALUES(?,?,?,?)";
        jdbcTemplate.update(query, unionOrWardModel.getDivisionId(),unionOrWardModel.getDistrictId(),unionOrWardModel.getThanaId(),unionOrWardModel.getUnionOrWardName());

    }

    public List<UnionOrWardModel> getAllUnionOrWards() {
        String unionORWardSql = "SELECT * FROM po_unions_or_wards";
        RowMapper<UnionOrWardModel> rowMapper = new UnionOrWardRowMapper();
        try{
            List<UnionOrWardModel> unionOrWardModelList = jdbcTemplate.query(unionORWardSql, rowMapper);
            return unionOrWardModelList;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }

    public UnionOrWardModel findUnionOrWardById(int id) {
        String query = "SELECT * FROM po_unions_or_wards WHERE id = ?";
        RowMapper<UnionOrWardModel> rowMapper = new UnionOrWardRowMapper();
        UnionOrWardModel unionOrWardModel = (UnionOrWardModel)jdbcTemplate.queryForObject(query, new Object[] { id }, rowMapper);
        return unionOrWardModel;
    }
    public void editUnionOrWard(UnionOrWardModel unionOrWardModel) {
        String query = "UPDATE po_unions_or_wards SET name = ? WHERE id=?";
        jdbcTemplate.update(query, unionOrWardModel.getUnionOrWardName(), unionOrWardModel.getUnionOrWardId());

    }


    public void deleteUnionOrWard(int id) {
        String query = "DELETE FROM po_unions_or_wards WHERE id=?";
        jdbcTemplate.update(query, id);
    }
    public List<UnionOrWardModel> findUnionOrWardByThanaId(int id) {
        String sql = "SELECT * FROM po_unions_or_wards where thana_id=?";
        RowMapper<UnionOrWardModel> rowMapper = new UnionOrWardRowMapper();
        try{
            List<UnionOrWardModel> unionOrWardModelList = jdbcTemplate.query(sql,new Object[] {id},rowMapper);
            return unionOrWardModelList;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }
}
