package com.springMicrofin360.microfin.repository;

import com.springMicrofin360.microfin.model.WorkingAreaModel;
import com.springMicrofin360.microfin.model.rowmapper.WorkingAreaRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class WorkingAreaRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    public void addWorkingArea(WorkingAreaModel workingAreaModel) {
        String query = "INSERT INTO po_working_areas(village_or_block_id,name,branch_id,code) VALUES(?,?,?,?)";
        jdbcTemplate.update(query, workingAreaModel.getVillageOrBlockId(),workingAreaModel.getWorkingAreaName(),workingAreaModel.getBranchId(),workingAreaModel.getWorkingAreaCode());

    }

    public List<WorkingAreaModel> getAllWorkingAreas() {
        String Sql = "SELECT * FROM po_working_areas";
        RowMapper<WorkingAreaModel> rowMapper = new WorkingAreaRowMapper();
        try{
            List<WorkingAreaModel> workingAreaModelList = jdbcTemplate.query(Sql, rowMapper);
            return workingAreaModelList;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }

    public WorkingAreaModel findWorkingAreaById(int id) {
        String query = "SELECT * FROM po_working_areas WHERE id = ?";
        RowMapper<WorkingAreaModel> rowMapper = new WorkingAreaRowMapper();
        WorkingAreaModel workingAreaModel = (WorkingAreaModel) jdbcTemplate.queryForObject(query, new Object[] { id }, rowMapper);
        return workingAreaModel;
    }
    public void editWorkingArea(WorkingAreaModel workingAreaModel) {
        String query = "UPDATE po_working_areas SET name = ?,code = ? WHERE id=?";
        jdbcTemplate.update(query, workingAreaModel.getWorkingAreaName(),workingAreaModel.getWorkingAreaCode(), workingAreaModel.getWorkingAreaId());

    }


    public void deleteWorkingArea(int id) {
        String query = "DELETE FROM po_working_areas WHERE id=?";
        jdbcTemplate.update(query, id);
    }

}
