package com.springMicrofin360.microfin.repository;
import com.springMicrofin360.microfin.model.ThanaModel;
import com.springMicrofin360.microfin.model.rowmapper.ThanaRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ThanaRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    public void addThana(ThanaModel thanaModel) {
        String query = "INSERT INTO po_thanas(division_id,district_id,name) VALUES(?,?,?)";
        jdbcTemplate.update(query, thanaModel.getDivisionId(),thanaModel.getDistrictId(),thanaModel.getThanaName());

    }

    public List<ThanaModel> getAllThanas() {
        String thanaSql = "SELECT * FROM po_thanas";
        RowMapper<ThanaModel> rowMapper = new ThanaRowMapper();
        try{
            List<ThanaModel> thanas = jdbcTemplate.query(thanaSql, rowMapper);
            return thanas;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }

    public ThanaModel findThanaById(int id) {
        String query = "SELECT * FROM po_thanas WHERE id = ?";
        RowMapper<ThanaModel> rowMapper = new ThanaRowMapper();
        ThanaModel thana = (ThanaModel)jdbcTemplate.queryForObject(query, new Object[] { id }, rowMapper);
        return thana;
    }
    public void editThana(ThanaModel thanaModel) {
        String query = "UPDATE po_thanas SET name = ? WHERE id=?";
        jdbcTemplate.update(query, thanaModel.getThanaName(), thanaModel.getThanaId());

    }


    public void deleteThana(int id) {
        String query = "DELETE FROM po_thanas WHERE id=?";
        jdbcTemplate.update(query, id);
    }
    public List<ThanaModel> findThanaByDistrictId(int id) {
        String thanaSql = "SELECT * FROM po_thanas where district_id=?";
        RowMapper<ThanaModel> rowMapper = new ThanaRowMapper();
        try{
            List<ThanaModel> thanas = jdbcTemplate.query(thanaSql,new Object[] {id},rowMapper);
            return thanas;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }
}
