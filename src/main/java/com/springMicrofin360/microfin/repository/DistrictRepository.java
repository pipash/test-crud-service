package com.springMicrofin360.microfin.repository;

import com.springMicrofin360.microfin.model.DistrictModel;
import com.springMicrofin360.microfin.model.rowmapper.DistrictRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class DistrictRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    public void addDistrict(DistrictModel districtModel) {
        String query = "INSERT INTO po_districts(division_id,name) VALUES(?,?)";
        jdbcTemplate.update(query, districtModel.getDivisionId(),districtModel.getDistrictName());

    }

    public List<DistrictModel> getAllDistricts() {
        String districtSql = "SELECT * FROM po_districts";
        RowMapper<DistrictModel> rowMapper = new DistrictRowMapper();
        try{
            List<DistrictModel> districts = jdbcTemplate.query(districtSql, rowMapper);
            return districts;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }

    public DistrictModel findDistrictById(int id) {
        String query = "SELECT * FROM po_districts WHERE id = ?";
        RowMapper<DistrictModel> rowMapper = new DistrictRowMapper();
        DistrictModel district = (DistrictModel)jdbcTemplate.queryForObject(query, new Object[] { id }, rowMapper);
        return district;
    }
    public void editDistrict(DistrictModel districtModel) {
        String query = "UPDATE po_districts SET name = ? WHERE id=?";
        jdbcTemplate.update(query, districtModel.getDistrictName(), districtModel.getDistrictId());

    }


    public void deleteDistrict(int id) {
        String query = "DELETE FROM po_districts WHERE id=?";
        jdbcTemplate.update(query, id);
    }

    public List<DistrictModel> findDistrictByDivisionId(int id) {
        String districtSql = "SELECT * FROM po_districts where division_id=?";
        RowMapper<DistrictModel> rowMapper = new DistrictRowMapper();
        try{
            List<DistrictModel> districts = jdbcTemplate.query(districtSql,new Object[] {id},rowMapper);
            return districts;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }
}
