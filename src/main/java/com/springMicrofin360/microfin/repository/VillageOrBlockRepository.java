package com.springMicrofin360.microfin.repository;

import com.springMicrofin360.microfin.model.VillageOrBlockModel;
import com.springMicrofin360.microfin.model.rowmapper.VillageOrBlockRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class VillageOrBlockRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    public void addVillageOrBlock(VillageOrBlockModel villageOrBlockModel) {
        String query = "INSERT INTO po_village_or_blocks(division_id,district_id,thana_id,union_or_ward_id,name) VALUES(?,?,?,?,?)";
        jdbcTemplate.update(query, villageOrBlockModel.getDivisionId(),villageOrBlockModel.getDistrictId(),villageOrBlockModel.getThanaId(),villageOrBlockModel.getUnionOrWardId(),villageOrBlockModel.getVillageOrBlockName());

    }

    public List<VillageOrBlockModel> getAllVillageOrBlocks() {
        String Sql = "SELECT * FROM po_village_or_blocks";
        RowMapper<VillageOrBlockModel> rowMapper = new VillageOrBlockRowMapper();
        try{
            List<VillageOrBlockModel> villageOrBlockModelList = jdbcTemplate.query(Sql, rowMapper);
            return villageOrBlockModelList;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }

    public VillageOrBlockModel findVillageOrBlockById(int id) {
        String query = "SELECT * FROM po_village_or_blocks WHERE id = ?";
        RowMapper<VillageOrBlockModel> rowMapper = new VillageOrBlockRowMapper();
        VillageOrBlockModel villageOrBlockModel = (VillageOrBlockModel)jdbcTemplate.queryForObject(query, new Object[] { id }, rowMapper);
        return villageOrBlockModel;
    }
    public void editVillageOrBlock(VillageOrBlockModel villageOrBlockModel) {
        String query = "UPDATE po_village_or_blocks SET name = ? WHERE id=?";
        jdbcTemplate.update(query, villageOrBlockModel.getVillageOrBlockName(), villageOrBlockModel.getVillageOrBlockId());

    }


    public void deleteVillageOrBlock(int id) {
        String query = "DELETE FROM po_village_or_blocks WHERE id=?";
        jdbcTemplate.update(query, id);
    }

    public List<VillageOrBlockModel> findVillageOrBlockByUnionOrWard(int id) {
        String sql = "SELECT * FROM po_village_or_blocks where union_or_ward_id=?";
        RowMapper<VillageOrBlockModel> rowMapper = new VillageOrBlockRowMapper();
        try{
            List<VillageOrBlockModel> villageOrBlockModelList = jdbcTemplate.query(sql,new Object[] {id},rowMapper);
            return villageOrBlockModelList;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }
}
