package com.springMicrofin360.microfin.service;

import com.springMicrofin360.microfin.model.UnionOrWardModel;
import com.springMicrofin360.microfin.repository.UnionOrWardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnionOrWardService {
    @Autowired
    private UnionOrWardRepository unionOrWardRepository;

    public void addUnionOrWard(UnionOrWardModel unionOrWardModel){
        unionOrWardRepository.addUnionOrWard(unionOrWardModel);
    }

    public List<UnionOrWardModel> getAllUnionOrWards(){

        return unionOrWardRepository.getAllUnionOrWards();
    }

    public UnionOrWardModel findUnionOrWardById(int id){

        return unionOrWardRepository.findUnionOrWardById(id);
    }

    public void editUnionOrWard(UnionOrWardModel unionOrWardModel){

        unionOrWardRepository.editUnionOrWard(unionOrWardModel);
    }

    public void deleteUnionOrWard(int id){

        unionOrWardRepository.deleteUnionOrWard(id);
    }
    public List<UnionOrWardModel> findUnionOrWardByThanaId(int id){
        return unionOrWardRepository.findUnionOrWardByThanaId(id);
    }
}
