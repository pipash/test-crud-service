package com.springMicrofin360.microfin.service;

import com.springMicrofin360.microfin.model.DistrictModel;
import com.springMicrofin360.microfin.repository.DistrictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class DistrictService {
    @Autowired
    private DistrictRepository districtRepository;

    public void addDistrict(DistrictModel districtModel){
        districtRepository.addDistrict(districtModel);
    }

    public List<DistrictModel> getAllDistricts(){

        return districtRepository.getAllDistricts();
    }

    public DistrictModel findDistrictById(int id){

        return districtRepository.findDistrictById(id);
    }

    public void editDistrict(DistrictModel districtModel){

        districtRepository.editDistrict(districtModel);
    }

    public void deleteDistrict(int id){

        districtRepository.deleteDistrict(id);
    }


    public List<DistrictModel> findDistrictByDivisionId(int id){
        return districtRepository.findDistrictByDivisionId(id);
    }
}
