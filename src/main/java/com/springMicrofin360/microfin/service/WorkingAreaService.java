package com.springMicrofin360.microfin.service;

import com.springMicrofin360.microfin.model.WorkingAreaModel;
import com.springMicrofin360.microfin.repository.WorkingAreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkingAreaService {
    @Autowired
    private WorkingAreaRepository workingAreaRepository;

    public void addWorkingArea(WorkingAreaModel workingAreaModel){
        workingAreaRepository.addWorkingArea(workingAreaModel);
    }

    public List<WorkingAreaModel> getAllWorkingAreas(){

        return workingAreaRepository.getAllWorkingAreas();
    }

    public WorkingAreaModel findWorkingAreaById(int id){

        return workingAreaRepository.findWorkingAreaById(id);
    }

    public void editWorkingArea(WorkingAreaModel workingAreaModel){

        workingAreaRepository.editWorkingArea(workingAreaModel);
    }

    public void deleteWorkingArea(int id){

        workingAreaRepository.deleteWorkingArea(id);
    }

}
