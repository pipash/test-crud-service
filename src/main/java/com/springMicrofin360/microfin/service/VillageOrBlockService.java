package com.springMicrofin360.microfin.service;

import com.springMicrofin360.microfin.model.VillageOrBlockModel;
import com.springMicrofin360.microfin.repository.VillageOrBlockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VillageOrBlockService {
    @Autowired
    private VillageOrBlockRepository villageOrBlockRepository;

    public void addVillageOrBlock(VillageOrBlockModel villageOrBlockModel){
        villageOrBlockRepository.addVillageOrBlock(villageOrBlockModel);
    }

    public List<VillageOrBlockModel> getAllVillageOrBlocks(){

        return villageOrBlockRepository.getAllVillageOrBlocks();
    }

    public VillageOrBlockModel findVillageOrBlockById(int id){

        return villageOrBlockRepository.findVillageOrBlockById(id);
    }

    public void editVillageOrBlock(VillageOrBlockModel villageOrBlockModel){

        villageOrBlockRepository.editVillageOrBlock(villageOrBlockModel);
    }

    public void deleteVillageOrBlock(int id){

        villageOrBlockRepository.deleteVillageOrBlock(id);
    }
    public List<VillageOrBlockModel> findVillageOrBlockByUnionOrWard(int id){
        return villageOrBlockRepository.findVillageOrBlockByUnionOrWard(id);
    }
}
