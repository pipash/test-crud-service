package com.springMicrofin360.microfin.service;

import com.springMicrofin360.microfin.model.EmployeeDepartmentModel;
import com.springMicrofin360.microfin.repository.EmployeeDepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeDepartmentService {
    @Autowired
    private EmployeeDepartmentRepository employeeDepartmentRepository;

    public void addEmployeeDepartmrnt(EmployeeDepartmentModel employeeDepartmentModel){
        employeeDepartmentRepository.addEmployeeDepartMent(employeeDepartmentModel);
    }

    public List<EmployeeDepartmentModel> GetAllEmployeeDepartment(){
        return employeeDepartmentRepository.getAllEmployeeDepaerments();
    }
}
