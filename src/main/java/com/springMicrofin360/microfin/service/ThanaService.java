package com.springMicrofin360.microfin.service;
import com.springMicrofin360.microfin.model.ThanaModel;
import com.springMicrofin360.microfin.repository.ThanaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ThanaService {
    @Autowired
    private ThanaRepository thanaRepository;

    public void addThana(ThanaModel thanaModel){
        thanaRepository.addThana(thanaModel);
    }

    public List<ThanaModel> getAllThanas(){

        return thanaRepository.getAllThanas();
    }

    public ThanaModel findThanaById(int id){

        return thanaRepository.findThanaById(id);
    }

    public void editThana(ThanaModel thanaModel){

        thanaRepository.editThana(thanaModel);
    }

    public void deleteThana(int id){

        thanaRepository.deleteThana(id);
    }
    public List<ThanaModel> findThanaByDistrictId(int id){
        return thanaRepository.findThanaByDistrictId(id);
    }
}
