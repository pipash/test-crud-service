package com.springMicrofin360.microfin.model.rowmapper;

import com.springMicrofin360.microfin.model.WorkingAreaModel;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WorkingAreaRowMapper  implements RowMapper<WorkingAreaModel> {
    public WorkingAreaModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        WorkingAreaModel workingAreaModel = new WorkingAreaModel();
        workingAreaModel.setWorkingAreaId(rs.getInt("id"));
        workingAreaModel.setVillageOrBlockId(rs.getInt("village_or_block_id"));
        workingAreaModel.setWorkingAreaName(rs.getString("name"));
        workingAreaModel.setBranchId(rs.getInt("branch_id"));
        workingAreaModel.setWorkingAreaCode(rs.getString("code"));
        return workingAreaModel;

    }
}
