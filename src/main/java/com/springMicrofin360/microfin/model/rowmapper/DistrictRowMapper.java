package com.springMicrofin360.microfin.model.rowmapper;

import com.springMicrofin360.microfin.model.DistrictModel;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DistrictRowMapper implements RowMapper<DistrictModel> {

    public DistrictModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        DistrictModel districtModel = new DistrictModel();
        districtModel.setDistrictId(rs.getInt("id"));
        districtModel.setDivisionId(rs.getInt("division_id"));
        districtModel.setDistrictName(rs.getString("name"));
        return districtModel;
    }
}
