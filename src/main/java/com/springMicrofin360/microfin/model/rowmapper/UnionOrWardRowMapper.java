package com.springMicrofin360.microfin.model.rowmapper;

import com.springMicrofin360.microfin.model.UnionOrWardModel;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UnionOrWardRowMapper implements RowMapper<UnionOrWardModel> {
    public UnionOrWardModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        UnionOrWardModel unionOrWardModel = new UnionOrWardModel();
        unionOrWardModel.setUnionOrWardId(rs.getInt("id"));
        unionOrWardModel.setDivisionId(rs.getInt("division_id"));
        unionOrWardModel.setDistrictId(rs.getInt("district_id"));
        unionOrWardModel.setThanaId(rs.getInt("thana_id"));
        unionOrWardModel.setUnionOrWardName(rs.getString("name"));
        return unionOrWardModel;
    }
}
