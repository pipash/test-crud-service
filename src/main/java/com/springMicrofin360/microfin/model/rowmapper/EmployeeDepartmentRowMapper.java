package com.springMicrofin360.microfin.model.rowmapper;


import com.springMicrofin360.microfin.model.EmployeeDepartmentModel;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeDepartmentRowMapper implements RowMapper<EmployeeDepartmentModel> {
    @Override
    public EmployeeDepartmentModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        EmployeeDepartmentModel employeeDepartmentModel = new EmployeeDepartmentModel();
        employeeDepartmentModel.setDepartmentId(rs.getInt("id"));
        employeeDepartmentModel.setDepartmentCode(rs.getString("code"));
        employeeDepartmentModel.setDepartmentName(rs.getString("name"));
        return employeeDepartmentModel;
    }
}
