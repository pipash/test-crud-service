package com.springMicrofin360.microfin.model.rowmapper;
import com.springMicrofin360.microfin.model.ThanaModel;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
public class ThanaRowMapper implements RowMapper<ThanaModel>{
    public ThanaModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        ThanaModel districtModel = new ThanaModel();
        districtModel.setThanaId(rs.getInt("id"));
        districtModel.setDivisionId(rs.getInt("division_id"));
        districtModel.setDistrictId(rs.getInt("district_id"));
        districtModel.setThanaName(rs.getString("name"));
        return districtModel;
    }
}
