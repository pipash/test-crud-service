package com.springMicrofin360.microfin.model.rowmapper;


import com.springMicrofin360.microfin.model.VillageOrBlockModel;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VillageOrBlockRowMapper implements RowMapper<VillageOrBlockModel> {
    public VillageOrBlockModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        VillageOrBlockModel villageOrBlockModel = new VillageOrBlockModel();
        villageOrBlockModel.setVillageOrBlockId(rs.getInt("id"));
        villageOrBlockModel.setDivisionId(rs.getInt("division_id"));
        villageOrBlockModel.setDistrictId(rs.getInt("district_id"));
        villageOrBlockModel.setThanaId(rs.getInt("thana_id"));
        villageOrBlockModel.setVillageOrBlockName(rs.getString("name"));
        return villageOrBlockModel;

    }
}
