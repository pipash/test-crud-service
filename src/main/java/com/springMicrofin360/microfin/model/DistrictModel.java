package com.springMicrofin360.microfin.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class DistrictModel {
    private int districtId;
    @NotNull
    @NotEmpty(message = "District name required")
    private String districtName;
    private int divisionId;
    public Integer getDistrictId() {
        return districtId;
    }
    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }
    public String getDistrictName() {
        return districtName;
    }
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
    public int getDivisionId() {
        return divisionId;
    }
    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

}
