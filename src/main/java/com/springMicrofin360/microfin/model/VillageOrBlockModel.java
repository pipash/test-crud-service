package com.springMicrofin360.microfin.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class VillageOrBlockModel {
    private int villageOrBlockId;
    @NotNull
    @NotEmpty(message = "Village Or block name required")
    private String villageOrBlockName;
    private int divisionId;
    private int districtId;
    private int thanaId;
    private int unionOrWardId;

    public int getVillageOrBlockId() {
        return villageOrBlockId;
    }
    public void setVillageOrBlockId(int villageOrBlockId) {
        this.villageOrBlockId = villageOrBlockId;
    }

    public String getVillageOrBlockName() {
        return villageOrBlockName;
    }
    public void setVillageOrBlockName(String villageOrBlockName) {
        this.villageOrBlockName = villageOrBlockName;
    }

    public int getUnionOrWardId(){return unionOrWardId;}
    public void setUnionOrWardId(int unionOrWardId){this.unionOrWardId = unionOrWardId;}

    public int getThanaId() {
        return thanaId;
    }
    public void setThanaId(int thanaId) {
        this.thanaId = thanaId;
    }

    public int getDistrictId() {
        return districtId;
    }
    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getDivisionId() {
        return divisionId;
    }
    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }
}
