package com.springMicrofin360.microfin.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ThanaModel {
    private int thanatId;
    @NotNull
    @NotEmpty(message = "Thana name required")
    private String thanaName;
    private int divisionId;
    private int districtId;
    public int getThanaId() {
        return thanatId;
    }
    public void setThanaId(int thanatId) {
        this.thanatId = thanatId;
    }
    public String getThanaName() {
        return thanaName;
    }
    public void setThanaName(String thanaName) {
        this.thanaName = thanaName;
    }
    public int getDistrictId() {
        return districtId;
    }
    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getDivisionId() {
        return divisionId;
    }
    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }
}
