package com.springMicrofin360.microfin.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UnionOrWardModel {
    private int unionOrWardId;
    @NotNull
    @NotEmpty(message = "Union Or Ward name required")
    private String unionOrWardName;
    private int divisionId;
    private int districtId;
    private int thanaId;

    public int getUnionOrWardId(){return unionOrWardId;}
    public void setUnionOrWardId(int unionOrWardId){this.unionOrWardId = unionOrWardId;}
    public String getUnionOrWardName() {
        return unionOrWardName;
    }
    public void setUnionOrWardName(String unionOrWardName) {
        this.unionOrWardName = unionOrWardName;
    }

    public int getThanaId() {
        return thanaId;
    }
    public void setThanaId(int thanaId) {
        this.thanaId = thanaId;
    }

    public int getDistrictId() {
        return districtId;
    }
    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getDivisionId() {
        return divisionId;
    }
    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }
}
