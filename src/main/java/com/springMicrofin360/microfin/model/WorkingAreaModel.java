package com.springMicrofin360.microfin.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class WorkingAreaModel {
    private int workingAreaId;
    @NotNull
    @NotEmpty(message = "Working Area required")
    private int villageOrBlockId;
    private int branchId;
    private String workingAreaCode;
    private String workingAreaName;

    public int getWorkingAreaId() {
        return workingAreaId;
    }
    public void setWorkingAreaId(int workingAreaId) {
        this.workingAreaId = workingAreaId;
    }

    public int getBranchId() {
        return branchId;
    }
    public void setBranchId(int branchId){
        this.branchId = branchId;
    }
    public int getVillageOrBlockId(){
        return villageOrBlockId;
    }
    public void setVillageOrBlockId(int villageOrBlockId){
        this.villageOrBlockId = villageOrBlockId;
    }
    public String getWorkingAreaCode(){
        return workingAreaCode;
    }
    public void setWorkingAreaCode(String workingAreaCode){
        this.workingAreaCode = workingAreaCode;
    }
    public String getWorkingAreaName(){
        return workingAreaName;
    }
    public void setWorkingAreaName(String workingAreaName){
        this.workingAreaName = workingAreaName;
    }
}
